# version

This package is useful to add version to an executable.

## `Makefile` example

```Makefile
.DEFAULT_GOAL	:= build

SHELL 		:= /bin/bash
BINDIR		:= bin
DOCKERDIR	:= docker
RELEASEDIR  := release
OUTPUT_NAME := mark
GOHOSTOS 	:= $(shell go env GOHOSTOS)

ifndef GOOS
    GOOS := $(GOHOSTOS)
endif

ifndef GOARCH
	GOARCH := $(shell go env GOHOSTARCH)
endif

GOFILES		= $(shell find . -type f -name '*.go' -not -path "./vendor/*")
GODIRS		= $(shell go list -f '{{.Dir}}' ./...)
GOPKGS		= $(shell go list ./...)

APP_VER		:= $(shell git describe --always 2> /dev/null || echo "unknown")

BUILD_SYM	:= gitlab.com/doniapr-gopkg/pkg/version
# If using vendor, use the following BUILD_SYM instead
# BUILD_SYM	:= gitlab.linkaja.com/be/namor/vendor/gitlab.com/doniapr-gopkg/pkg/version

LDFLAGS		+= -X $(BUILD_SYM).version=$(APP_VER)
LDFLAGS		+= -X $(BUILD_SYM).gitRevision=$(shell git rev-parse --short HEAD 2> /dev/null  || echo unknown)
LDFLAGS		+= -X $(BUILD_SYM).branch=$(shell git rev-parse --abbrev-ref HEAD 2> /dev/null  || echo unknown)
LDFLAGS		+= -X $(BUILD_SYM).buildUser=$(shell whoami || echo nobody)@$(shell hostname -f || echo builder)
LDFLAGS		+= -X $(BUILD_SYM).buildDate=$(shell date +%Y-%m-%dT%H:%M:%S%:z)
LDFLAGS		+= -X $(BUILD_SYM).goVersion=$(word 3,$(shell go version))
LDFLAGS		+= -s -w

.PHONY: build
build:
	@go build -ldflags '$(LDFLAGS)' -o $(BINDIR)/$(OUTPUT_NAME)

.PHONY: test
test-vstb:
	@go test -v gitlab.linkaja.com/be/namor

.PHONY: clean
clean:
	@echo "--> cleaning compiled objects and binaries"
	@go clean -tags netgo -i $(GOPKGS)
	@rm -rf $(BINDIR)/*
	@rm -rf $(RELEASEDIR)/*
```

## `main.go` example

```go
package main

import (
    "fmt"

    "gitlab.com/doniapr-gopkg/pkg/version"
)

func main() {
    fmt.Printf("%v\n", version.Build.Version)
}
```