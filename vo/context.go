package vo

import (
	"net/http"
	"reflect"

	"github.com/labstack/echo/v4"
	Error "gitlab.com/doniapr-gopkg/pkg/error"
	"gitlab.com/doniapr-gopkg/pkg/json"
	Response "gitlab.com/doniapr-gopkg/pkg/response"
	Session "gitlab.com/doniapr-gopkg/pkg/session"
)

const AppSession = "App_Session"

type ApplicationContext struct {
	echo.Context
	Session Session.Session
}

func Parse(c echo.Context) *ApplicationContext {
	data := c.Get(AppSession)
	session := data.(Session.Session)
	return &ApplicationContext{Context: c, Session: session}
}

// - validate payload
func (c *ApplicationContext) BindRequest(requestModel interface{}) error {
	timeProcess := c.Session.T2("ApplicationContext:BindRequest")

	if err := c.Bind(requestModel); err != nil {
		return Error.New(Response.ErrorInvalidJson, err.Error())
	}

	if err := c.Validate(requestModel); err != nil {
		return Error.New(Response.ErrorInvalidJson, err.Error())
	}

	c.Session.T3(timeProcess, requestModel)
	return nil
}

// - Response
func (c *ApplicationContext) Ok(data interface{}) error {
	if data == nil {
		data = struct{}{}
	}

	response := Response.DefaultResponse{
		Response: Response.Response{
			Status:  Response.SuccessCode,
			Message: "Success",
		},
		Data: data,
	}

	c.Session.T4(response)

	return c.Context.JSON(http.StatusOK, response)
}

func serializeCurrency(data interface{}) interface{} {
	c, _ := json.Marshal(data)
	dataLog := map[string]map[string]interface{}{}

	// Just return if response cannot be unmarshall
	err := json.Unmarshal(c, &dataLog)
	if err != nil {
		return data
	}

	currency := dataLog["data"]["currency"]

	if reflect.TypeOf(currency).String() != "string" {
		b, _ := json.Marshal(currency)
		b, _ = json.Marshal(string(b))
		dataLog["data"]["currency"] = string(b)
	}

	return dataLog
}

func (c *ApplicationContext) Response(status string, message string, data interface{}) error {

	if data == nil {
		data = struct{}{}
	}

	response := Response.DefaultResponse{
		Response: Response.Response{
			Status:  status,
			Message: message,
		},
		Data: data,
	}

	c.Session.T4(response)
	return c.Context.JSON(http.StatusOK, response)
}

func (c *ApplicationContext) Error(err error, data interface{}) error {
	code := Response.GeneralError

	if data == nil {
		data = struct{}{}
	}

	response := Response.DefaultResponse{
		Response: Response.Response{
			Status:  code,
			Message: "error",
		},
		Data: data,
	}

	if he, ok := err.(*Error.ApplicationError); ok {
		response.Status = he.ErrorCode
		response.Message = he.Error()
	} else if he, ok := err.(*echo.HTTPError); ok {
		response.Message = he.Error()
	} else {
		response.Message = err.Error()
	}

	c.Session.T4(response)
	return c.Context.JSON(http.StatusOK, response)
}

// - Response
func (c *ApplicationContext) Raw(status int, response interface{}) error {
	c.Session.T4(response)

	return c.Context.JSON(status, response)
}
