package http

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	Handler "gitlab.com/doniapr-gopkg/pkg/transport/example/http/handler"
	Router "gitlab.com/doniapr-gopkg/pkg/transport/example/http/router"
	Middleware "gitlab.com/doniapr-gopkg/pkg/transport/http/middleware"
)

func TestWithInherit(t *testing.T) {
	h := New([]Option{
		WithInherit(true),
	})

	assert.Equal(t, true, h.inherit)
}

func TestWithHost(t *testing.T) {
	host := "linkaja.id"

	h := New([]Option{
		WithHost(host),
	})

	assert.Equal(t, host, h.host)
}

func TestWithPort(t *testing.T) {
	port := 80

	h := New([]Option{
		WithPort(port),
	})

	assert.Equal(t, port, h.port)
}

func TestWithAddress(t *testing.T) {
	host := "linkaja.id"
	port := 80

	h := New([]Option{
		WithAddress(host, port),
	})

	assert.Equal(t, host, h.host)
	assert.Equal(t, port, h.port)
}

func TestWithDebug(t *testing.T) {
	h := New([]Option{
		WithDebug(true),
	})

	assert.Equal(t, true, h.debug)
}

func TestWithLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	h := New([]Option{
		WithLogger(logger),
	})

	assert.Equal(t, logger, h.logger)
}

func TestWithTLS(t *testing.T) {
	certificateFile := "certificate.pem"
	keyFile := "key.pem"
	caFile := "ca.pem"
	serverName := "linkaja.id"

	h := New([]Option{
		WithTLS(certificateFile, keyFile, caFile, serverName, true),
	})

	assert.Equal(t, true, h.tls)
	assert.Equal(t, false, h.h2c)
	assert.Equal(t, certificateFile, h.certificateFile)
	assert.Equal(t, keyFile, h.keyFile)
	assert.Equal(t, caFile, h.rootCAFile)
	assert.Equal(t, serverName, h.serverName)
	assert.Equal(t, true, h.insecureSkipVerify)
}

func TestWithH2C(t *testing.T) {
	maxConcurrentStreams := uint32(500)
	maxReadFrameSize := uint32(1048576)

	h := New([]Option{
		WithH2C(true, maxConcurrentStreams, maxReadFrameSize),
	})

	assert.Equal(t, true, h.h2c)
	assert.Equal(t, false, h.tls)
	assert.Equal(t, maxConcurrentStreams, h.h2cMaxConcurrentStreams)
	assert.Equal(t, maxReadFrameSize, h.h2cMaxReadFrameSize)
}

func TestWithTimeout(t *testing.T) {
	readTimeout := 5 * time.Second
	writeTimeout := 5 * time.Second
	idleTimeout := 5 * time.Second

	h := New([]Option{
		WithTimeout(readTimeout, writeTimeout, idleTimeout),
	})

	assert.Equal(t, readTimeout, h.readTimeout)
	assert.Equal(t, writeTimeout, h.writeTimeout)
	assert.Equal(t, idleTimeout, h.idleTimeout)
}

func TestWithKeepAlive(t *testing.T) {
	h := New([]Option{
		WithKeepAlive(true),
	})

	assert.Equal(t, true, h.keepAliveEnabled)
}

func TestWithGracefulShutdownTime(t *testing.T) {
	gracefulShutdownTime := 5 * time.Second

	h := New([]Option{
		WithGracefulShutdownTime(gracefulShutdownTime),
	})

	assert.Equal(t, gracefulShutdownTime, h.gracefulShutdownTime)
}

func TestWithRouter(t *testing.T) {
	handler := Handler.New()
	router := Router.New(handler)

	h := New([]Option{
		WithRouter(router),
	})

	assert.Equal(t, router, h.router)
}

func TestWithMiddleware(t *testing.T) {
	middleware := Middleware.New([]Middleware.Option{})

	h := New([]Option{
		WithMiddleware(middleware),
	})

	assert.Equal(t, middleware, h.middleware)
}

func TestWithTracing(t *testing.T) {
	tracingName := "LinkAja"
	jaegerHost := "127.0.0.1"
	jaegerPort := uint16(5775)
	probability := 0.01

	h := New([]Option{
		WithTracing(tracingName, jaegerHost, jaegerPort, probability),
	})

	assert.Equal(t, true, h.tracing)
	assert.Equal(t, tracingName, h.tracingName)
	assert.Equal(t, jaegerHost, h.jaegerHost)
	assert.Equal(t, jaegerPort, h.jaegerPort)
	assert.Equal(t, probability, h.tracingProbability)
}
