package middleware

import (
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
)

func TestWithLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	m := New([]Option{
		WithLogger(logger),
	})

	assert.Equal(t, logger, m.logger)
}

func TestWithDebug(t *testing.T) {
	m := New([]Option{
		WithDebug(true),
	})

	assert.Equal(t, true, m.debug)
}

func TestWithProfiling(t *testing.T) {
	m := New([]Option{
		WithProfiling(true),
	})

	assert.Equal(t, true, m.profiling)
}

func TestWithRecover(t *testing.T) {
	m := New([]Option{
		WithRecover(true),
	})

	assert.Equal(t, true, m.recover)
}

func TestWithCORS(t *testing.T) {
	m := New([]Option{
		WithCORS(true),
	})

	assert.Equal(t, true, m.cors)
}

func TestWithGZip(t *testing.T) {
	m := New([]Option{
		WithGZip(true),
	})

	assert.Equal(t, true, m.gzip)
}

func TestWithValidator(t *testing.T) {
	validatorData := validator.New()

	m := New([]Option{
		WithValidator(true, validatorData),
	})

	assert.Equal(t, true, m.validator)
	assert.Equal(t, validatorData, m.validatorData)
}

func TestWithCustomValidationErrorMessage(t *testing.T) {
	//customValidationErrorMessage := func(interface{}) error { return nil }
	//
	//m := New([]Option{
	//	WithCustomValidationErrorMessage(customValidationErrorMessage),
	//})
	//
	//assert.Equal(t, customValidationErrorMessage, m.customValidationErrorMessage)
}

func TestWithErrorHandler(t *testing.T) {
	m := New([]Option{
		WithErrorHandler(true),
	})

	assert.Equal(t, true, m.errorHandler)
}

func TestWithAcceptJSON(t *testing.T) {
	m := New([]Option{
		WithAcceptJSON(true),
	})

	assert.Equal(t, true, m.acceptJSON)
}

func TestWithSession(t *testing.T) {
	port := 80
	name := "LinkAja"
	version := "0.0.1"

	m := New([]Option{
		WithSession(true, name, version, port),
	})

	assert.Equal(t, true, m.session)
	assert.Equal(t, name, m.name)
	assert.Equal(t, version, m.version)
	assert.Equal(t, port, m.port)
}

func TestWithInternalServerErrorMessage(t *testing.T) {
	internalServerErrorMessage := "Server Gangguan!"

	m := New([]Option{
		WithInternalServerErrorMessage(internalServerErrorMessage),
	})

	assert.Equal(t, internalServerErrorMessage, m.internalServerErrorMessage)
}

func TestWithSkip(t *testing.T) {
	urls := []string{"/admin"}

	m := New([]Option{
		WithSkip(urls),
	})

	assert.Equal(t, urls[0], m.skipURLs[0])
}

func TestWithHealth(t *testing.T) {
	url := "sehat"

	m := New([]Option{
		WithHealth(url),
	})

	assert.Equal(t, true, m.health)
	assert.Equal(t, url, m.healthURL)
	assert.Equal(t, url, m.skipURLs[0])
}

func TestWithAvailability(t *testing.T) {
	url := "toggle"

	m := New([]Option{
		WithAvailability(url),
	})

	assert.Equal(t, true, m.availabilityEnabled)
	assert.Equal(t, url, m.availabilityURLPrefix)
	assert.Equal(t, url, m.skipURLs[0])
}

func TestWithAvailabilityCredential(t *testing.T) {
	username := "admin"
	password := "admin"

	m := New([]Option{
		WithAvailabilityCredential(username, password),
	})

	assert.Equal(t, username, m.availabilityUsername)
	assert.Equal(t, password, m.availabilityPassword)
}

func TestWithEndpointAvailability(t *testing.T) {
	endpoints := []string{"/admin"}

	m := New([]Option{
		WithEndpointAvailability(endpoints),
	})

	assert.Equal(t, endpoints[0], m.endpointAvailabilityURLs[0])
}

func TestWithCORSConfig(t *testing.T) {
	corsConfig := middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{echo.GET, echo.POST, echo.PUT, echo.DELETE, echo.PATCH, echo.HEAD, echo.OPTIONS},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "token", "Pv", echo.HeaderContentType, "Accept", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
		AllowCredentials: true,
	}

	m := New([]Option{
		WithCORSConfig(corsConfig),
	})

	assert.Equal(t, true, m.corsConfigEnabled)
	assert.Equal(t, corsConfig, m.corsConfig)
}
