package middleware

import (
	"bytes"
	"crypto/subtle"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	_ "net/http/pprof"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go/log"
	"gitlab.com/doniapr-gopkg/pkg/transport/http/tracer"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	Error "gitlab.com/doniapr-gopkg/pkg/error"
	"gitlab.com/doniapr-gopkg/pkg/json"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	Response "gitlab.com/doniapr-gopkg/pkg/response"
	Session "gitlab.com/doniapr-gopkg/pkg/session"
	Utils "gitlab.com/doniapr-gopkg/pkg/utils"
	ValueObject "gitlab.com/doniapr-gopkg/pkg/vo"
	"go.uber.org/zap"
)

const (
	InternalServerErrorMessage = "Internal Server Error"
	Name                       = "LinkAja"
	Version                    = "1.0.0"
	RequestTime                = "RequestTime"
	RequestID                  = "RequestID"
	JourneyID                  = "JourneyID"
	ClientID                   = "ClientID"
	RequestError               = "RequestError"
	AlreadyLogged              = "AlreadyLogged"
	DebugURL                   = "/debug/pprof/*"
	HeaderXID                  = "xid"
	HeaderJID                  = "jid"
	HeaderCID                  = "cid"
)

type CustomValidationErrorMessage func(interface{}) error

type Middleware struct {
	logger                                                                                                                                             Logger.Logger
	port                                                                                                                                               int
	name, version, internalServerErrorMessage, healthURL, availabilityUsername, availabilityPassword, availabilityURLPrefix, requestIDIn, requestIDOut string
	debug, recover, cors, gzip, validator, errorHandler, session, acceptJSON, health, profiling, available, availabilityEnabled, corsConfigEnabled     bool
	skipURLs, endpointAvailabilityURLs                                                                                                                 []string
	endpointAvailabilityMap                                                                                                                            map[string]bool
	validatorData                                                                                                                                      *validator.Validate
	customValidationErrorMessage                                                                                                                       CustomValidationErrorMessage
	corsConfig                                                                                                                                         middleware.CORSConfig
}

func New(opts []Option) *Middleware {
	m := &Middleware{
		port:                       80,
		name:                       Name,
		version:                    Version,
		internalServerErrorMessage: InternalServerErrorMessage,
		available:                  true,
		endpointAvailabilityMap:    make(map[string]bool),
	}

	for _, opt := range opts {
		opt(m)
	}

	if m.logger == nil {
		m.logger = Logger.Noop()
	}

	if m.errorHandler || m.recover {
		m.errorHandler = true
		m.recover = true
	}

	if m.profiling {
		m.skipURLs = append(m.skipURLs, DebugURL[0:len(DebugURL)-2])
	}

	if m.availabilityEnabled {
		m.skipURLs = append(m.skipURLs, m.availabilityURLPrefix)

		if len(m.endpointAvailabilityURLs) > 0 {
			for _, path := range m.endpointAvailabilityURLs {
				m.endpointAvailabilityMap[path] = true
			}
		}
	}

	return m
}

func (m *Middleware) SetPort(port int) {
	m.port = port
}

func (m *Middleware) SetLogger(logger Logger.Logger) {
	m.logger = logger
}

func (m *Middleware) SetDebug(enabled bool) {
	m.debug = enabled
}

func (m *Middleware) Setup(e *echo.Echo) {

	e.Pre(middleware.RemoveTrailingSlash())

	if m.profiling {
		e.GET(DebugURL, echo.WrapHandler(http.DefaultServeMux))
	}

	if m.health {
		e.GET(m.healthURL, func(c echo.Context) error {
			return m.healthHandler(c)
		})
	}

	if m.availabilityEnabled {
		// group toggle endpoint
		toggle := e.Group(m.availabilityURLPrefix)

		// set basic auth
		if len(m.availabilityUsername) > 0 || len(m.availabilityPassword) > 0 {
			toggle.Use(middleware.BasicAuth(func(username string, password string, context echo.Context) (b bool, err error) {
				if subtle.ConstantTimeCompare([]byte(username), []byte(m.availabilityUsername)) == 1 &&
					subtle.ConstantTimeCompare([]byte(password), []byte(m.availabilityPassword)) == 1 {
					return true, nil
				}

				return false, nil
			}))
		}

		// toggle all
		toggle.POST("", func(c echo.Context) error {
			m.available = !m.available

			if m.debug {
				m.logger.Info(fmt.Sprintf("availability status for HTTP server [%s %s] at [:%d] is %v", m.name, m.version, m.port, m.available))
			}

			response := make(map[string]bool)
			response["available"] = m.available

			return c.JSON(200, Response.DefaultResponse{
				Data: response,
				Response: Response.Response{
					Status:  Response.SuccessCode,
					Message: http.StatusText(http.StatusOK),
				},
			})
		})

		// get availability status
		toggle.GET("/_stats", func(c echo.Context) error {

			response := make(map[string]bool)
			response["*"] = m.available

			if len(m.endpointAvailabilityURLs) > 0 {
				for _, path := range m.endpointAvailabilityURLs {
					var available bool
					var ok bool
					if available, ok = m.endpointAvailabilityMap[path]; !ok {
						available = false
					}
					response[path] = available
				}
			}

			return c.JSON(200, Response.DefaultResponse{
				Data: response,
				Response: Response.Response{
					Status:  Response.SuccessCode,
					Message: http.StatusText(http.StatusOK),
				},
			})
		})

		// toggle by url
		if len(m.endpointAvailabilityURLs) > 0 {
			for _, path := range m.endpointAvailabilityURLs {
				toggle.POST(path, func(c echo.Context) error {
					path = strings.ReplaceAll(c.Path(), m.availabilityURLPrefix, "")

					// check if available
					var available bool
					var ok bool
					if available, ok = m.endpointAvailabilityMap[path]; !ok {
						available = false
					}
					// negate the status
					m.endpointAvailabilityMap[path] = !available

					if m.debug {
						m.logger.Info(fmt.Sprintf("availability status for endpoint [%s] on HTTP server [%s %s] at [:%d] is %v", path, m.name, m.version, m.port, m.endpointAvailabilityMap[path]))
					}

					response := make(map[string]interface{})
					response["path"] = path
					response["available"] = m.endpointAvailabilityMap[path]

					return c.JSON(200, Response.DefaultResponse{
						Data: response,
						Response: Response.Response{
							Status:  Response.SuccessCode,
							Message: http.StatusText(http.StatusOK),
						},
					})
				})
			}
		}
	}

	e.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) (err error) {

			c.Set(RequestTime, time.Now())

			requestID := c.Request().Header.Get(HeaderXID)
			if len(requestID) == 0 {
				requestID = c.Request().Header.Get(echo.HeaderXRequestID)
				if len(requestID) == 0 {
					requestID = Utils.GenerateThreadId()
				}
			}

			journeyID := c.Request().Header.Get(HeaderJID)
			if len(journeyID) == 0 {
				journeyID = requestID
			}

			clientID := c.Request().Header.Get(HeaderCID)
			if len(clientID) == 0 {
				clientID = requestID
			}

			c.Set(RequestID, requestID)
			c.Set(JourneyID, JourneyID)
			c.Set(ClientID, ClientID)

			if !m.skip(c) {
				var serverSpan opentracing.Span
				if serverSpan, err = tracer.ServerSpan(c); err != nil {
					return
				}

				var headers []log.Field
				for k, v := range c.Request().Header {
					headers = append(headers, log.String(k, strings.Join(v, ", ")))
				}
				serverSpan.LogFields(headers...)

				serverSpan.SetTag("xid", requestID)
				serverSpan.LogFields(log.String("version", m.version))
				serverSpan.LogFields(log.String("sourceIP", c.RealIP()))
				serverSpan.LogFields(log.String("method", c.Request().Method))

				if sc, ok := serverSpan.Context().(jaeger.SpanContext); ok {
					traceID := sc.TraceID().High
					if traceID == 0 {
						traceID = sc.TraceID().Low
					}
					uberTraceID := fmt.Sprintf("%016x", traceID)
					c.Response().Header().Set(jaeger.TraceContextHeaderName, uberTraceID)
				}

				c.Set(tracer.HTTPTracerServerSpan, serverSpan)
			}

			// check if availability toggle is active and if it is unavailable and not in the skip list
			if m.availabilityEnabled && !m.available && !m.skip(c) {
				return c.JSON(http.StatusServiceUnavailable, Response.DefaultResponse{
					Response: Response.Response{
						Status:  Response.GeneralError,
						Message: http.StatusText(http.StatusServiceUnavailable),
					},
				})
			}

			// check if server availability toggle is enabled
			if m.availabilityEnabled {
				// check if we have configurations for specific url
				if len(m.endpointAvailabilityURLs) > 0 {
					// check only for url not in skipped list
					if !m.skip(c) {
						// go through the url list
						for _, path := range m.endpointAvailabilityURLs {
							// we go by prefix here
							if strings.HasPrefix(c.Path(), path) {
								// check if url is available
								var available bool
								var ok bool
								if available, ok = m.endpointAvailabilityMap[path]; !ok {
									available = false
								}
								// if not available return error
								if !available {
									return c.JSON(http.StatusServiceUnavailable, Response.DefaultResponse{
										Response: Response.Response{
											Status:  Response.GeneralError,
											Message: http.StatusText(http.StatusServiceUnavailable),
										},
									})
								}
							}
						}
					}
				}
			}

			if m.session {
				// - Set session to context
				request, errRequest := hookRequest(c)
				if errRequest != nil {
					err = errRequest
					return
				}

				session := Session.New(m.logger).
					SetThreadID(requestID).
					SetAppName(m.name).
					SetAppVersion(m.version).
					SetPort(m.port).
					SetIP(c.Request().RemoteAddr).
					SetSrcIP(c.RealIP()).
					SetURL(c.Request().URL.String()).
					SetMethod(c.Request().Method).
					SetRequest(string(request)).
					SetHeader(formatHeader(c))

				if !m.skip(c) {
					session.T1("Incoming Request")
				}

				c.Set(ValueObject.AppSession, *session)
			}

			if !m.session {
				logRecord := []zap.Field{
					zap.String("_app_tag", "T1"),
					zap.String("_app_thread_id", requestID),
					zap.String("_app_method", c.Request().Method),
					zap.String("_app_uri", c.Request().URL.String()),
				}
				msg := Logger.FormatLog("_message_", "Incoming Request")
				logRecord = append(logRecord, msg)

				m.logger.Info("|", logRecord...)
			}

			c.Response().Header().Set(echo.HeaderXRequestID, requestID)
			c.Response().Header().Set(HeaderXID, requestID)
			c.Response().Header().Set(HeaderJID, journeyID)
			c.Response().Header().Set(HeaderCID, clientID)

			if m.acceptJSON {
				if !m.skip(c) {
					if c.Request().Header.Get(echo.HeaderContentType) != echo.MIMEApplicationJSON {
						return echo.NewHTTPError(http.StatusUnsupportedMediaType, http.StatusText(http.StatusUnsupportedMediaType))
					}
				}
			}

			if err = h(c); err != nil {
				c.Error(err)
				c.Response().Committed = true
				return err
			}
			return
		}
	})

	if m.gzip {
		e.Use(middleware.Gzip())
	}

	if m.recover {
		e.Use(middleware.Recover())
	}

	if m.cors || m.corsConfigEnabled {
		corsConfig := middleware.CORSConfig{
			AllowOrigins:     []string{"*"},
			AllowMethods:     []string{echo.GET, echo.POST, echo.PUT, echo.DELETE, echo.PATCH, echo.HEAD, echo.OPTIONS},
			AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "token", "Pv", echo.HeaderContentType, "Accept", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
			ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
			AllowCredentials: true,
		}

		if m.corsConfigEnabled {
			corsConfig = m.corsConfig
		}

		e.Use(middleware.CORSWithConfig(corsConfig))
	}

	if m.validator {
		e.Validator = NewDataValidator(m.validatorData, m.customValidationErrorMessage)
	}

	e.Use(middleware.BodyDump(func(c echo.Context, request []byte, response []byte) {
		var alreadyLogged bool
		var ok bool
		if alreadyLogged, ok = c.Get(AlreadyLogged).(bool); !ok {
			alreadyLogged = false
		}

		if !alreadyLogged {
			m.logRequest(c, request, response)
		}
	}))

	if m.errorHandler {
		e.HTTPErrorHandler = m.httpErrorHandler
	}

}

func (m *Middleware) logRequest(c echo.Context, request []byte, response []byte) {
	if m.health && strings.HasPrefix(c.Path(), m.healthURL) || m.skip(c) {
		return
	}

	if m.session {
		session := c.Get(ValueObject.AppSession).(Session.Session)

		var requestError error
		var ok bool
		if requestError, ok = c.Get(RequestError).(error); ok {
			session.SetErrorMessage(requestError.Error())
		}

		var resp map[string]interface{}
		if err := json.Unmarshal(response, &resp); err == nil {
			session.T4(resp)
		}
	}

	var requestID string
	var ok bool
	if requestID, ok = c.Get(RequestID).(string); !ok {
		requestID = Utils.GenerateThreadId()
	}

	if !m.session {

		var requestTime time.Time
		if requestTime, ok = c.Get(RequestTime).(time.Time); !ok {
			requestTime = time.Now()
		}

		tdrModel := Logger.LogTdrModel{
			AppName:    m.name,
			AppVersion: m.version,
			IP:         c.Request().RemoteAddr,
			Port:       m.port,
			SrcIP:      c.RealIP(),
			RespTime:   time.Now().Sub(requestTime).Nanoseconds() / 1000000,
			Path:       c.Path(),
			Header:     c.Request().Header,
			Request:    string(request),
			Response:   string(response),
			ThreadID:   requestID,
		}

		var requestError error
		if requestError, ok = c.Get(RequestError).(error); ok {
			tdrModel.Error = requestError.Error()
		}

		m.logger.TDR(tdrModel)
	}

	if tracer.HTTPTracer != nil {
		var serverSpan opentracing.Span
		var ok bool
		if serverSpan, ok = c.Get(tracer.HTTPTracerServerSpan).(opentracing.Span); ok {
			defer func() {
				serverSpan.Finish()
			}()

			if err := tracer.HTTPTracer.Inject(
				serverSpan.Context(),
				opentracing.HTTPHeaders,
				opentracing.HTTPHeadersCarrier(c.Response().Header()),
			); err != nil {
				return
			}

			if len(string(request)) > 0 {
				serverSpan.LogFields(log.String("request", string(request)))
			}
			if len(string(response)) > 0 {
				serverSpan.LogFields(log.String("response", string(response)))
			}

			serverSpan.SetTag("code", c.Response().Status)

			var requestError error
			if requestError, ok = c.Get(RequestError).(error); ok {
				serverSpan.SetTag("error", true)
				serverSpan.LogFields(log.String("error", requestError.Error()))
			} else {
				serverSpan.SetTag("error", false)
			}

		}

	}

}

func (m *Middleware) httpErrorHandler(err error, c echo.Context) {
	c.Set(RequestError, err)

	var (
		code    = http.StatusInternalServerError
		message = http.StatusText(code)
	)

	response := Response.DefaultResponse{
		Response: Response.Response{
			Status:  Response.GeneralError,
			Message: message,
		},
		Data: struct{}{},
	}

	if he, ok := err.(*Error.ApplicationError); ok {
		response.Status = he.ErrorCode
		response.Message = he.Message
	} else if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
		response.Message = he.Message.(string)
	} else {
		response.Message = err.Error()
	}

	if !c.Response().Committed {
		var responseError error
		if c.Request().Method == http.MethodHead { // Issue #608
			responseError = c.NoContent(code)
		} else {
			message := m.internalServerErrorMessage

			if strings.Contains(response.Message, http.StatusText(http.StatusUnsupportedMediaType)) {
				message = response.Message
				code = http.StatusUnsupportedMediaType
			} else if strings.Contains(response.Message, http.StatusText(http.StatusNotFound)) {
				message = response.Message
				code = http.StatusNotFound
			} else if strings.Contains(response.Message, http.StatusText(http.StatusMethodNotAllowed)) {
				message = response.Message
				code = http.StatusMethodNotAllowed
			} else if strings.Contains(response.Message, http.StatusText(http.StatusBadRequest)) {
				message = http.StatusText(http.StatusBadRequest)

				messages := strings.Split(response.Message, http.StatusText(http.StatusBadRequest))
				if len(messages) == 2 {
					if len(messages[1]) > 0 {
						message = messages[1]
						c.Set(RequestError, errors.New(message))
					}
				}
				code = http.StatusBadRequest
			} else if strings.Contains(response.Message, http.StatusText(http.StatusForbidden)) {
				message = response.Message
				code = http.StatusForbidden
			} else if strings.Contains(response.Message, http.StatusText(http.StatusUnauthorized)) {
				message = http.StatusText(http.StatusUnauthorized)
				code = http.StatusUnauthorized
			}

			response.Message = message
			responseError = c.JSON(code, response)
		}

		var alreadyLogged bool
		var ok bool
		if alreadyLogged, ok = c.Get(AlreadyLogged).(bool); !ok {
			alreadyLogged = false
		}

		if !alreadyLogged {
			c.Set(AlreadyLogged, true)

			responseByte, _ := json.Marshal(response)
			requestByte, _ := hookRequest(c)

			m.logRequest(c, requestByte, responseByte)
		}

		err = responseError
	}
}

func (m *Middleware) healthHandler(c echo.Context) error {
	content := c.QueryParam("content")
	from := c.Get("RequestTime").(time.Time)
	data := make(map[string]interface{})
	data["name"] = m.name
	data["version"] = m.version
	data["elapsed"] = time.Now().Sub(from).Nanoseconds() / 1000000
	response := Response.CreateResponse(Response.SuccessCode, "healthy", data)
	if content == "true" {
		return c.JSON(http.StatusOK, response)
	}
	return c.NoContent(http.StatusNoContent)
}

func (m *Middleware) skip(c echo.Context) (skip bool) {
	for _, url := range m.skipURLs {
		if strings.HasPrefix(strings.ToLower(c.Request().URL.String()), url) {
			skip = true
			return
		}
	}
	return
}

func hookRequest(c echo.Context) (body []byte, err error) {
	if c.Request().Body != nil { // Read
		body, err = ioutil.ReadAll(c.Request().Body)
		if err != nil {
			return body, err
		}
	}
	c.Request().Body = ioutil.NopCloser(bytes.NewBuffer(body))
	return body, err
}

func formatHeader(c echo.Context) (result map[string]interface{}) {
	result = make(map[string]interface{})
	if headers := c.Request().Header; headers != nil {
		for k, v := range headers {
			result[k] = v
		}
		return result
	}
	return
}

type DataValidator struct {
	ValidatorData                *validator.Validate
	customValidationErrorMessage CustomValidationErrorMessage
}

func NewDataValidator(validatorData *validator.Validate, customValidationErrorMessage CustomValidationErrorMessage) *DataValidator {
	return &DataValidator{ValidatorData: validatorData, customValidationErrorMessage: customValidationErrorMessage}
}

func (cv *DataValidator) Validate(i interface{}) error {
	if cv.customValidationErrorMessage == nil {
		return cv.ValidatorData.Struct(i)
	}

	return cv.customValidationErrorMessage(cv.ValidatorData.Struct(i))
}
