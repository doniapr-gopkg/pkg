package middleware

import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"testing"
)

func TestMiddleware_SetLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	m := New([]Option{})

	m.SetLogger(logger)

	assert.Equal(t, logger, m.logger)
}

func TestMiddleware_SetDebug(t *testing.T) {
	m := New([]Option{})

	m.SetDebug(true)

	assert.Equal(t, true, m.debug)
}

func TestMiddleware_SetPort(t *testing.T) {
	m := New([]Option{})

	port := 80

	m.SetPort(port)

	assert.Equal(t, port, m.port)
}

func TestMiddleware_Setup(t *testing.T) {
	m := New([]Option{})

	server := echo.New()

	m.Setup(server)

}
