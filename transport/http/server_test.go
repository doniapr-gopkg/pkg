package http

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	Middleware "gitlab.com/doniapr-gopkg/pkg/transport/http/middleware"
	"golang.org/x/sync/errgroup"
	"sync"
	"testing"
)

func TestServer_SetDebug(t *testing.T) {
	h := New([]Option{})

	h.SetDebug(true)

	assert.Equal(t, true, h.debug)
}

func TestServer_SetLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	h := New([]Option{})

	h.SetLogger(logger)

	assert.Equal(t, logger, h.logger)
}

func TestServer_Address(t *testing.T) {
	host := "linkaja.id"
	port := 80

	h := New([]Option{
		WithAddress(host, port),
	})

	assert.Equal(t, fmt.Sprintf("%s:%d", host, port), h.Address())
}

func TestServer_SetMiddleware(t *testing.T) {
	h := New([]Option{})

	middleware := Middleware.New([]Middleware.Option{})

	h.SetMiddleware(middleware)

	assert.Equal(t, middleware, h.middleware)
}

func TestServer_SetupMiddleware(t *testing.T) {
	h := New([]Option{})

	h.SetupMiddleware([]Middleware.Option{})

	assert.NotNil(t, h.middleware)
}

func TestServer_Start(t *testing.T) {
	h := New([]Option{})

	ctx, cancel := context.WithCancel(context.Background())

	eg, _ := errgroup.WithContext(context.Background())

	var wg sync.WaitGroup
	wg.Add(1)

	eg.Go(h.Start(ctx, &wg))

	cancel()

	err := eg.Wait()

	assert.Nil(t, err)
}
