package tracer

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/opentracing/opentracing-go/ext"

	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

const (
	HTTPTracerServerSpan = "HTTPTracerServerSpan"
)

var (
	HTTPTracer opentracing.Tracer
)

var (
	ErrHTTPTracerNotSet = errors.New("http tracer not set")
)

type spanFromEchoContextFunc func(spanContext context.Context, span opentracing.Span) error
type spanFromContextFunc func(spanContext context.Context, span opentracing.Span) (interface{}, error)
type spanFromContextFuncNoError func(spanContext context.Context, span opentracing.Span) interface{}

type nullCloser struct{}

func (*nullCloser) Close() error { return nil }

func New(enable bool, name, jaegerHost string, jaegerPort uint16, probability float64) (closer io.Closer) {
	if !enable {
		HTTPTracer = new(opentracing.NoopTracer)
		closer = new(nullCloser)
		return
	}

	cfg := config.Configuration{
		ServiceName: "HTTP_" + name,
		Sampler: &config.SamplerConfig{
			Type:  jaeger.SamplerTypeProbabilistic,
			Param: probability,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			LocalAgentHostPort:  fmt.Sprintf("%s:%d", jaegerHost, jaegerPort),
		},
	}

	var err error
	if HTTPTracer, closer, err = cfg.NewTracer(
		config.Logger(jaeger.NullLogger),
	); err != nil {
		HTTPTracer = new(opentracing.NoopTracer)
		closer = new(nullCloser)
		return
	}

	return
}

func ServerSpan(c echo.Context) (serverSpan opentracing.Span, err error) {
	if HTTPTracer == nil {
		err = ErrHTTPTracerNotSet
		return
	}

	if spanContext, err := HTTPTracer.Extract(
		opentracing.HTTPHeaders,
		opentracing.HTTPHeadersCarrier(c.Request().Header),
	); err != nil {
		serverSpan = HTTPTracer.StartSpan(c.Request().URL.Path, ext.RPCServerOption(spanContext))
		c.Set(HTTPTracerServerSpan, serverSpan)
	} else {
		serverSpan = HTTPTracer.StartSpan(c.Request().URL.Path)
		c.Set(HTTPTracerServerSpan, serverSpan)
	}

	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}
	}()

	return
}

func SpanFromEchoContext(c echo.Context, operationName string, f spanFromEchoContextFunc) error {
	var serverSpan opentracing.Span
	var ok bool
	var ctx context.Context
	if serverSpan, ok = c.Get(HTTPTracerServerSpan).(opentracing.Span); !ok {
		var err error
		if serverSpan, err = ServerSpan(c); err != nil {
			ctx = context.Background()
		} else {
			ctx = opentracing.ContextWithSpan(context.Background(), serverSpan)
		}
	} else {
		ctx = opentracing.ContextWithSpan(context.Background(), serverSpan)
	}

	span, spanContext := opentracing.StartSpanFromContextWithTracer(ctx, HTTPTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}

		ctx.Done()
		span.Finish()
	}()

	return f(spanContext, span)
}

func SpanFromContext(spanCtx context.Context, operationName string, f spanFromContextFunc) (interface{}, error) {
	span, ctx := opentracing.StartSpanFromContextWithTracer(spanCtx, HTTPTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}

		ctx.Done()
		span.Finish()
	}()

	return f(ctx, span)
}

func SpanFromContextNoError(spanCtx context.Context, operationName string, f spanFromContextFuncNoError) interface{} {
	span, ctx := opentracing.StartSpanFromContextWithTracer(spanCtx, HTTPTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}

		ctx.Done()
		span.Finish()
	}()

	return f(ctx, span)
}
