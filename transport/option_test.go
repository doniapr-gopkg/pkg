package transport

import (
	"testing"

	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/transport/custom"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc"
	"gitlab.com/doniapr-gopkg/pkg/transport/http"
)

func TestWithInherit(t *testing.T) {
	transport := New([]Option{
		WithInherit(true),
	})

	assert.Equal(t, true, transport.inherit)
}

func TestWithLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	transport := New([]Option{
		WithLogger(logger),
	})

	assert.Equal(t, logger, transport.logger)
}

func TestWithDebug(t *testing.T) {
	transport := New([]Option{
		WithDebug(true),
	})

	assert.Equal(t, true, transport.debug)
}

func TestWithHTTPServer(t *testing.T) {
	h := http.New([]http.Option{})

	transport := New([]Option{
		WithHTTPServer(h),
	})

	assert.Equal(t, h, transport.httpServer)
}

func TestWithGRPCServer(t *testing.T) {
	g := grpc.New([]grpc.Option{})

	transport := New([]Option{
		WithGRPCServer(g),
	})

	assert.Equal(t, g, transport.grpcServer)
}

func TestWithCustom(t *testing.T) {
	holder := custom.New([]custom.Option{})

	transport := New([]Option{
		WithCustom(holder),
	})

	assert.Equal(t, holder, transport.services[0])
}

func TestWithTracing(t *testing.T) {
	tracingName := "LinkAja"
	jaegerHost := "127.0.0.1"
	jaegerPort := uint16(5775)
	probability := 0.01

	h := New([]Option{
		WithTracing(tracingName, jaegerHost, jaegerPort, probability),
	})

	assert.Equal(t, true, h.tracing)
	assert.Equal(t, tracingName, h.tracingName)
	assert.Equal(t, jaegerHost, h.jaegerHost)
	assert.Equal(t, jaegerPort, h.jaegerPort)
	assert.Equal(t, probability, h.tracingProbability)
}
