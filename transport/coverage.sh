#!/usr/bin/env bash

set -e

echo 'mode: count' > profile.cov

go test -short -covermode=count -coverprofile=profile.tmp .
if [ -f profile.tmp ]; then
  cat profile.tmp | tail -n +2 >>profile.cov
  rm profile.tmp
fi

for dir in $(find . -maxdepth 10 \
  -not -path './.git*' \
  -not -path '*/_*' \
  -not -path '.' \
  -not -path './vendor*' \
  -not -path './example*' \
  -type d); do
  if ls $dir/*.go &>/dev/null; then
    go test -short -covermode=count -coverprofile=$dir/profile.tmp $dir
    if [ -f $dir/profile.tmp ]; then
      cat $dir/profile.tmp | tail -n +2 >>profile.cov
      rm $dir/profile.tmp
    fi
  fi
done

go tool cover -func profile.cov
