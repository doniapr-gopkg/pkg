package tracer

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

var (
	TransportTracer opentracing.Tracer
)

var (
	ErrTracerNotSet = errors.New("tracer not set")
)

type spanFromContextFunc func(spanContext context.Context, span opentracing.Span) (interface{}, error)
type spanFromContextFuncNoError func(spanContext context.Context, span opentracing.Span) interface{}

type nullCloser struct{}

func (*nullCloser) Close() error { return nil }

func New(enable bool, name, jaegerHost string, jaegerPort uint16, probability float64) (closer io.Closer) {
	if !enable {
		TransportTracer = new(opentracing.NoopTracer)
		closer = new(nullCloser)
		return
	}

	cfg := config.Configuration{
		ServiceName: name,
		Sampler: &config.SamplerConfig{
			Type:  jaeger.SamplerTypeProbabilistic,
			Param: probability,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			LocalAgentHostPort:  fmt.Sprintf("%s:%d", jaegerHost, jaegerPort),
		},
	}

	var err error
	if TransportTracer, closer, err = cfg.NewTracer(
		config.Logger(jaeger.NullLogger),
	); err != nil {
		TransportTracer = new(opentracing.NoopTracer)
		closer = new(nullCloser)
		return
	}

	return
}

func Span(operationName string) (span opentracing.Span, err error) {
	if TransportTracer == nil {
		err = ErrTracerNotSet
		return
	}

	span = TransportTracer.StartSpan(operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}
	}()

	return span, nil
}

func SpanFromSpan(applicationContext context.Context, span opentracing.Span, operationName string, f spanFromContextFunc) (interface{}, error) {
	ctx := opentracing.ContextWithSpan(applicationContext, span)
	span, spanContext := opentracing.StartSpanFromContextWithTracer(ctx, TransportTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}
		ctx.Done()
		span.Finish()
	}()

	return f(spanContext, span)
}

func SpanFromSpanNoError(applicationContext context.Context, span opentracing.Span, operationName string, f spanFromContextFuncNoError) interface{} {
	ctx := opentracing.ContextWithSpan(applicationContext, span)
	span, spanContext := opentracing.StartSpanFromContextWithTracer(ctx, TransportTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}
		ctx.Done()
		span.Finish()
	}()

	return f(spanContext, span)
}

func SpanFromContext(spanCtx context.Context, operationName string, f spanFromContextFunc) (interface{}, error) {
	if TransportTracer == nil {
		panic(ErrTracerNotSet)
	}

	span, ctx := opentracing.StartSpanFromContextWithTracer(spanCtx, TransportTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}
		ctx.Done()
		span.Finish()
	}()

	return f(ctx, span)
}

func SpanFromContextNoError(spanCtx context.Context, operationName string, f spanFromContextFuncNoError) interface{} {
	if TransportTracer == nil {
		panic(ErrTracerNotSet)
	}

	span, ctx := opentracing.StartSpanFromContextWithTracer(spanCtx, TransportTracer, operationName)
	defer func() {
		if err := recover(); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "system panic: %v\n", err)
			return
		}
		ctx.Done()
		span.Finish()
	}()

	return f(ctx, span)
}
