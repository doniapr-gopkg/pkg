package main

import (
	"fmt"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/transport"
	Handler "gitlab.com/doniapr-gopkg/pkg/transport/example/grpc/handler"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc"
	Interceptor "gitlab.com/doniapr-gopkg/pkg/transport/grpc/interceptor"
	"time"
)

func main() {
	logger := Logger.New(Logger.Options{
		FileLocation:    "tdr.log",
		FileTdrLocation: "log.log",
		FileMaxAge:      time.Hour,
		Stdout:          true,
	})

	interceptor := Interceptor.New([]Interceptor.Option{})

	handler := Handler.New()

	g := grpc.New([]grpc.Option{
		grpc.WithInherit(true),
		grpc.WithUnary(true),
		grpc.WithInterceptor(interceptor),
		grpc.WithHandler(handler),
	})

	t := transport.New([]transport.Option{
		transport.WithDebug(true),
		transport.WithLogger(logger),
		transport.WithInherit(true),
		transport.WithGRPCServer(g),
	})

	if err := t.Run(); err != nil {
		logger.Error(fmt.Sprintf("error transport : %+v", err))
	}
}
