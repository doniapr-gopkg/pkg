package handler

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/doniapr-gopkg/pkg/transport/http/tracer"

	"github.com/labstack/echo/v4"
	Response "gitlab.com/doniapr-gopkg/pkg/response"
)

const (
	ShowContentKey   = "content"
	ShowContentValue = "true"
)

type ResponseData struct {
	Elapsed int64 `json:"elapsed"`
}

type healthHandler struct{}

func NewHealthHandler() *healthHandler {
	return &healthHandler{}
}

func (h *healthHandler) defaultResponse(ctx echo.Context) *ResponseData {
	requestTime := ctx.Get("RequestTime").(time.Time)
	stop := time.Now()

	return &ResponseData{Elapsed: stop.Sub(requestTime).Nanoseconds() / 1000000}
}

func (h *healthHandler) Ping(ctx echo.Context) (err error) {
	return tracer.SpanFromEchoContext(ctx, "ping", func(spanContext context.Context, span opentracing.Span) error {
		span.SetTag("bias", "tegaralaga")

		content := ctx.QueryParam(ShowContentKey)
		if content == ShowContentValue {
			return ctx.JSON(http.StatusOK, h.Ping2(ctx, spanContext))
		}

		return ctx.NoContent(http.StatusNoContent)
	})
}

func (h *healthHandler) Ping2(echoCtx echo.Context, spanCtx context.Context) *Response.DefaultResponse {
	result := tracer.SpanFromContextNoError(spanCtx, "ping2", func(spanContext context.Context, span opentracing.Span) interface{} {
		span.SetTag("kaisar", "bumi")

		return &Response.DefaultResponse{
			Data: h.defaultResponse(echoCtx),
			Response: Response.Response{
				Status:  Response.SuccessCode,
				Message: http.StatusText(http.StatusOK),
			},
		}
	})

	return result.(*Response.DefaultResponse)
}

func (h *healthHandler) PingError(ctx echo.Context) (err error) {
	content := ctx.QueryParam(ShowContentKey)

	var app []string

	if content == app[3] {
		return ctx.JSON(http.StatusOK, &Response.DefaultResponse{
			Data: h.defaultResponse(ctx),
			Response: Response.Response{
				Status:  Response.SuccessCode,
				Message: http.StatusText(http.StatusOK),
			},
		})
	}

	return ctx.NoContent(http.StatusNoContent)
}
