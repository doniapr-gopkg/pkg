package main

import (
	"fmt"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/transport"
	HTTPHandler "gitlab.com/doniapr-gopkg/pkg/transport/example/http/handler"
	HTTPRouter "gitlab.com/doniapr-gopkg/pkg/transport/example/http/router"
	"gitlab.com/doniapr-gopkg/pkg/transport/http"
	HTTPMiddleware "gitlab.com/doniapr-gopkg/pkg/transport/http/middleware"
)

func main() {
	logger := Logger.New(Logger.Options{
		FileLocation:    "tdr.log",
		FileTdrLocation: "log.log",
		FileMaxAge:      time.Hour,
		Stdout:          true,
	})

	httpHandler := HTTPHandler.New()

	httpRouter := HTTPRouter.New(httpHandler)

	httpMiddleware := HTTPMiddleware.New([]HTTPMiddleware.Option{
		HTTPMiddleware.WithHealth("/ping"),
		HTTPMiddleware.WithRecover(true),
		HTTPMiddleware.WithErrorHandler(true),
		HTTPMiddleware.WithAvailability("/hello"),
		HTTPMiddleware.WithEndpointAvailability([]string{"/health/error", "/health"}),
		HTTPMiddleware.WithAvailabilityCredential("admin", "admin"),
		HTTPMiddleware.WithCORSConfig(middleware.CORSConfig{
			AllowOrigins:     []string{"*"},
			AllowMethods:     []string{echo.GET, echo.POST, echo.PUT, echo.DELETE, echo.PATCH, echo.HEAD, echo.OPTIONS},
			AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "token", "Pv", echo.HeaderContentType, "Accept", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
			ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin"},
			AllowCredentials: true,
		}),
		//HTTPMiddleware.WithProfiling(true),
	})

	httpServer := http.New([]http.Option{
		http.WithInherit(true),
		http.WithPort(8765),
		http.WithMiddleware(httpMiddleware),
		http.WithRouter(httpRouter),
		http.WithH2C(true, 750, 1048576),
		http.WithTimeout(5*time.Second, 10*time.Second, 0*time.Second),
		http.WithTracing("anduril", "localhost", 5775, 0.01),
	})

	t := transport.New([]transport.Option{
		transport.WithInherit(true),
		transport.WithDebug(true),
		transport.WithLogger(logger),
		transport.WithHTTPServer(httpServer),
		transport.WithTracing("anduril", "localhost", 5775, 0.01),
	})

	if err := t.Run(); err != nil {
		logger.Error(fmt.Sprintf("error transport : %+v", err))
	}
}
