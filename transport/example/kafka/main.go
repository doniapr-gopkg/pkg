package main

import (
	"fmt"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/transport"
	"gitlab.com/doniapr-gopkg/pkg/transport/custom"
	"gitlab.com/doniapr-gopkg/pkg/transport/example/kafka/service"
	"time"
)

func main() {
	logger := Logger.New(Logger.Options{
		FileLocation:    "tdr.log",
		FileTdrLocation: "log.log",
		FileMaxAge:      time.Hour,
		Stdout:          true,
	})

	k := service.New()
	holder := custom.New([]custom.Option{
		custom.WithService(k),
	})

	t := transport.New([]transport.Option{
		transport.WithDebug(true),
		transport.WithLogger(logger),
		transport.WithInherit(true),
		transport.WithCustom(holder),
	})

	if err := t.Run(); err != nil {
		logger.Error(fmt.Sprintf("error transport : %+v", err))
	}
}
