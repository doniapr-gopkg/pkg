package main

import (
	"fmt"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/transport"
	"gitlab.com/doniapr-gopkg/pkg/transport/custom"
	Handler "gitlab.com/doniapr-gopkg/pkg/transport/example/grpc/handler"
	HTTPHandler "gitlab.com/doniapr-gopkg/pkg/transport/example/http/handler"
	HTTPRouter "gitlab.com/doniapr-gopkg/pkg/transport/example/http/router"
	"gitlab.com/doniapr-gopkg/pkg/transport/example/kafka/service"
	"gitlab.com/doniapr-gopkg/pkg/transport/example/multi/ulang"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc"
	Interceptor "gitlab.com/doniapr-gopkg/pkg/transport/grpc/interceptor"
	"gitlab.com/doniapr-gopkg/pkg/transport/http"
	HTTPMiddleware "gitlab.com/doniapr-gopkg/pkg/transport/http/middleware"
	"time"
)

func main() {
	logger := Logger.New(Logger.Options{
		FileLocation:    "tdr.log",
		FileTdrLocation: "log.log",
		FileMaxAge:      time.Hour,
		Stdout:          true,
	})

	// http service
	httpHandler := HTTPHandler.New()

	httpRouter := HTTPRouter.New(httpHandler)

	httpMiddleware := HTTPMiddleware.New([]HTTPMiddleware.Option{})

	httpServer := http.New([]http.Option{
		http.WithInherit(true),
		http.WithPort(8765),
		http.WithMiddleware(httpMiddleware),
		http.WithRouter(httpRouter),
	})

	// gRPC service
	interceptor := Interceptor.New([]Interceptor.Option{})

	handler := Handler.New()

	g := grpc.New([]grpc.Option{
		grpc.WithInherit(true),
		grpc.WithPort(4321),
		grpc.WithUnary(true),
		grpc.WithInterceptor(interceptor),
		grpc.WithHandler(handler),
	})

	// kafka service
	k := service.New()
	kafkaHolder := custom.New([]custom.Option{
		custom.WithService(k),
	})

	// ulang service
	u := ulang.New()
	ulangHolder := custom.New([]custom.Option{
		custom.WithService(u),
	})

	// transport
	t := transport.New([]transport.Option{
		transport.WithInherit(true),
		transport.WithDebug(true),
		transport.WithLogger(logger),
		transport.WithHTTPServer(httpServer),
		transport.WithGRPCServer(g),
		transport.WithCustom(kafkaHolder),
		transport.WithCustom(ulangHolder),
	})

	if err := t.Run(); err != nil {
		logger.Error(fmt.Sprintf("error transport : %+v", err))
	}
}
