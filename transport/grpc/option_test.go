package grpc

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	Handler "gitlab.com/doniapr-gopkg/pkg/transport/example/grpc/handler"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc/interceptor"
	"google.golang.org/grpc/keepalive"
)

func TestWithStream(t *testing.T) {
	g := New([]Option{
		WithStream(true),
	})

	assert.Equal(t, true, g.stream)
}

func TestWithUnary(t *testing.T) {
	g := New([]Option{
		WithUnary(true),
	})

	assert.Equal(t, true, g.unary)
}

func TestWithKeepAliveEnforcementPolicy(t *testing.T) {
	minTime := 5 * time.Second

	kaep := keepalive.EnforcementPolicy{
		MinTime:             minTime,
		PermitWithoutStream: true,
	}

	g := New([]Option{
		WithKeepAliveEnforcementPolicy(kaep),
	})

	assert.Equal(t, minTime, g.keepAlivePolicy.MinTime)
	assert.Equal(t, true, g.keepAlivePolicy.PermitWithoutStream)
	assert.Equal(t, kaep, g.keepAlivePolicy)
}

func TestWithKeepAliveServerParameters(t *testing.T) {
	maxConnectionIdle := 5 * time.Second
	maxConnectionAge := 6 * time.Second
	maxConnectionAgeGrace := 7 * time.Second
	spTime := 8 * time.Second
	timeout := 9 * time.Second

	kasp := keepalive.ServerParameters{
		MaxConnectionIdle:     maxConnectionIdle,
		MaxConnectionAge:      maxConnectionAge,
		MaxConnectionAgeGrace: maxConnectionAgeGrace,
		Time:                  spTime,
		Timeout:               timeout,
	}

	g := New([]Option{
		WithKeepAliveServerParameters(kasp),
	})

	assert.Equal(t, maxConnectionIdle, g.keepAliveServerParameters.MaxConnectionIdle)
	assert.Equal(t, maxConnectionAge, g.keepAliveServerParameters.MaxConnectionAge)
	assert.Equal(t, maxConnectionAgeGrace, g.keepAliveServerParameters.MaxConnectionAgeGrace)
	assert.Equal(t, spTime, g.keepAliveServerParameters.Time)
	assert.Equal(t, timeout, g.keepAliveServerParameters.Timeout)
}

func TestWithInherit(t *testing.T) {
	g := New([]Option{
		WithInherit(true),
	})

	assert.Equal(t, true, g.inherit)
}

func TestWithInterceptor(t *testing.T) {
	i := interceptor.New([]interceptor.Option{})

	g := New([]Option{
		WithInterceptor(i),
	})

	assert.Equal(t, i, g.interceptor)
}

func TestWithHandler(t *testing.T) {
	handler := Handler.New()

	g := New([]Option{
		WithHandler(handler),
	})

	assert.Equal(t, handler, g.handler)
}

func TestWithHost(t *testing.T) {
	host := "linkaja.id"

	g := New([]Option{
		WithHost(host),
	})

	assert.Equal(t, host, g.host)
}

func TestWithPort(t *testing.T) {
	port := 80

	g := New([]Option{
		WithPort(port),
	})

	assert.Equal(t, port, g.port)
}

func TestWithAddress(t *testing.T) {
	host := "linkaja.id"
	port := 80

	g := New([]Option{
		WithAddress(host, port),
	})

	assert.Equal(t, host, g.host)
	assert.Equal(t, port, g.port)
}

func TestWithDebug(t *testing.T) {
	g := New([]Option{
		WithDebug(true),
	})

	assert.Equal(t, true, g.debug)
}

func TestWithLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	g := New([]Option{
		WithLogger(logger),
	})

	assert.Equal(t, logger, g.logger)
}

func TestWithTLS(t *testing.T) {
	certificateFile := "certificate.pem"
	keyFile := "key.pem"
	caFile := "ca.pem"
	serverName := "linkaja.id"

	g := New([]Option{
		WithTLS(certificateFile, keyFile, caFile, serverName, true),
	})

	assert.Equal(t, true, g.tls)
	assert.Equal(t, certificateFile, g.certificateFile)
	assert.Equal(t, keyFile, g.keyFile)
	assert.Equal(t, caFile, g.rootCAFile)
	assert.Equal(t, serverName, g.serverName)
	assert.Equal(t, true, g.insecureSkipVerify)
}

func TestWithTracing(t *testing.T) {
	tracingName := "LinkAja"
	jaegerHost := "127.0.0.1"
	jaegerPort := uint16(5775)
	probability := 0.01

	h := New([]Option{
		WithTracing(tracingName, jaegerHost, jaegerPort, probability),
	})

	assert.Equal(t, true, h.tracing)
	assert.Equal(t, tracingName, h.tracingName)
	assert.Equal(t, jaegerHost, h.jaegerHost)
	assert.Equal(t, jaegerPort, h.jaegerPort)
	assert.Equal(t, probability, h.tracingProbability)
}
