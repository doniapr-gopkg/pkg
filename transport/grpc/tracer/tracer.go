package tracer

import (
	"fmt"
	"io"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"

	"github.com/uber/jaeger-client-go/config"
)

var (
	GRPCTracer opentracing.Tracer
)

type nullCloser struct{}

func (*nullCloser) Close() error { return nil }

func New(enable bool, name, jaegerHost string, jaegerPort uint16, probability float64) (closer io.Closer) {
	if !enable {
		GRPCTracer = new(opentracing.NoopTracer)
		closer = new(nullCloser)
		return
	}

	cfg := config.Configuration{
		ServiceName: "HTTP_" + name,
		Sampler: &config.SamplerConfig{
			Type:  jaeger.SamplerTypeProbabilistic,
			Param: probability,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			LocalAgentHostPort:  fmt.Sprintf("%s:%d", jaegerHost, jaegerPort),
		},
	}

	var err error
	if GRPCTracer, closer, err = cfg.NewTracer(
		config.Logger(jaeger.NullLogger),
	); err != nil {
		GRPCTracer = new(opentracing.NoopTracer)
		closer = new(nullCloser)
		return
	}

	return
}
