package grpc

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	Interceptor "gitlab.com/doniapr-gopkg/pkg/transport/grpc/interceptor"
	"golang.org/x/sync/errgroup"
	"sync"
	"testing"
)

func TestServer_SetDebug(t *testing.T) {
	g := New([]Option{})

	g.SetDebug(true)

	assert.Equal(t, true, g.debug)
}

func TestServer_SetLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	g := New([]Option{})

	g.SetLogger(logger)

	assert.Equal(t, logger, g.logger)
}

func TestServer_Address(t *testing.T) {
	host := "linkaja.id"
	port := 80

	g := New([]Option{
		WithAddress(host, port),
	})

	assert.Equal(t, fmt.Sprintf("%s:%d", host, port), g.Address())
}

func TestServer_SetInterceptor(t *testing.T) {
	g := New([]Option{})

	interceptor := Interceptor.New([]Interceptor.Option{})

	g.SetInterceptor(interceptor)

	assert.Equal(t, interceptor, g.interceptor)
}

func TestServer_SetupInterceptor(t *testing.T) {
	g := New([]Option{})

	g.SetupInterceptor([]Interceptor.Option{})

	assert.NotNil(t, g.interceptor)
}

func TestServer_Start(t *testing.T) {
	g := New([]Option{})

	ctx, cancel := context.WithCancel(context.Background())

	eg, _ := errgroup.WithContext(context.Background())

	var wg sync.WaitGroup
	wg.Add(1)

	eg.Go(g.Start(ctx, &wg))

	cancel()

	err := eg.Wait()

	assert.NotNil(t, err)
}
