package grpc

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net"
	"sync"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	gRPCOpenTracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"gitlab.com/doniapr-gopkg/pkg/grpc/health"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	Handler "gitlab.com/doniapr-gopkg/pkg/transport/grpc/handler"
	Interceptor "gitlab.com/doniapr-gopkg/pkg/transport/grpc/interceptor"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc/tracer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
)

type Server struct {
	logger                                                                          Logger.Logger
	debug, inherit, tls, insecureSkipVerify, stream, unary, tracing                 bool
	port                                                                            int
	host, certificateFile, keyFile, rootCAFile, serverName, tracingName, jaegerHost string
	jaegerPort                                                                      uint16
	tracingProbability                                                              float64
	keepAlivePolicy                                                                 keepalive.EnforcementPolicy
	keepAliveServerParameters                                                       keepalive.ServerParameters
	handler                                                                         Handler.Handler
	interceptor                                                                     *Interceptor.Interceptor
}

func New(opts []Option) *Server {
	s := &Server{
		port:    2202,
		inherit: true,
	}

	for _, opt := range opts {
		opt(s)
	}

	if s.logger == nil {
		s.logger = Logger.Noop()
	}

	return s
}

func (s *Server) SetLogger(logger Logger.Logger) {
	s.logger = logger
}

func (s *Server) SetDebug(enabled bool) {
	s.debug = enabled
}

func (s *Server) Address() string {
	return fmt.Sprintf("%s:%d", s.host, s.port)
}

func (s *Server) SetInterceptor(interceptor *Interceptor.Interceptor) {
	s.interceptor = interceptor
}

func (s *Server) SetupInterceptor(opts []Interceptor.Option) {
	s.interceptor = Interceptor.New(opts)
}

func (s *Server) inheritInterceptor() {
	if s.inherit && s.interceptor != nil {
		s.interceptor.SetDebug(s.debug)
		s.interceptor.SetLogger(s.logger)
		s.interceptor.SetPort(s.port)
	}
}

func (s *Server) Start(ctx context.Context, wg *sync.WaitGroup) func() error {
	s.inheritInterceptor()
	return func() error {

		closer := tracer.New(s.tracing, s.tracingName, s.jaegerHost, s.jaegerPort, s.tracingProbability)

		if !s.unary && !s.stream {
			return fmt.Errorf("please specify grpc service method (unary &/ stream)")
		}

		var listener net.Listener
		var err error
		if listener, err = net.Listen("tcp", s.Address()); err != nil {
			return fmt.Errorf("%s already in use, error : %+v", s.Address(), err)
		}

		// initialize grpc options
		var options []grpc.ServerOption

		if s.tls {
			certificate, err := tls.LoadX509KeyPair(s.certificateFile, s.keyFile)
			if err != nil {
				return fmt.Errorf("could not load grpc certificates : %+v", certificate)
			}

			tlsConfig := &tls.Config{
				Certificates: []tls.Certificate{certificate},
			}

			if len(s.rootCAFile) > 0 {
				certPool := x509.NewCertPool()

				ca, err := ioutil.ReadFile(s.rootCAFile)
				if err != nil {
					return fmt.Errorf("could not load grpc ca authority : %+v", err)
				}

				if ok := certPool.AppendCertsFromPEM(ca); !ok {
					return fmt.Errorf("could not append grpc certs from pem")
				}
				tlsConfig.ServerName = s.serverName
				tlsConfig.RootCAs = certPool
				tlsConfig.InsecureSkipVerify = s.insecureSkipVerify
			}

			credential := credentials.NewTLS(tlsConfig)
			options = append(options, grpc.Creds(credential))
		}

		options = append(options, grpc.KeepaliveEnforcementPolicy(s.keepAlivePolicy))
		options = append(options, grpc.KeepaliveParams(s.keepAliveServerParameters))

		if s.interceptor != nil {
			if s.unary {
				if s.tracing {
					options = append(options, grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
						s.interceptor.Unary(),
						gRPCOpenTracing.UnaryServerInterceptor(gRPCOpenTracing.WithTracer(tracer.GRPCTracer)),
					)))
				} else {
					options = append(options, grpc.UnaryInterceptor(s.interceptor.Unary()))
				}
			}

			if s.stream {
				if s.tracing {
					options = append(options, grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
						s.interceptor.Stream(),
						gRPCOpenTracing.StreamServerInterceptor(gRPCOpenTracing.WithTracer(tracer.GRPCTracer)),
					)))
				} else {
					options = append(options, grpc.StreamInterceptor(s.interceptor.Stream()))
				}
			}
		}

		server := grpc.NewServer(options...)

		health.RegisterHealthServer(server)

		if s.handler != nil {
			s.handler.Register(server)
		}

		errorServer := make(chan error, 1)

		go func() {
			<-ctx.Done()
			server.GracefulStop()
			if s.tracing {
				if err := closer.Close(); err != nil {
					errorServer <- fmt.Errorf("error closing down tracing : %+v", err)
				}
			}
			close(errorServer)
			wg.Done()
		}()

		if s.debug {
			s.logger.Info(fmt.Sprintf("starting grpc server on %s", s.Address()))
		}

		if err := server.Serve(listener); err != nil {
			return fmt.Errorf("error starting grpc server : %+v", err)
		}

		if s.debug {
			s.logger.Info(fmt.Sprintf("grpc server stopped on %s", s.Address()))
		}

		err = <-errorServer
		wg.Wait()
		return err
	}
}
