package interceptor

import (
	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"testing"
)

func TestWithDebug(t *testing.T) {
	i := New([]Option{
		WithDebug(true),
	})

	assert.Equal(t, true, i.debug)
}

func TestWithLogger(t *testing.T) {
	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	i := New([]Option{
		WithLogger(logger),
	})

	assert.Equal(t, logger, i.logger)
}

func TestWithHandleCrash(t *testing.T) {
	i := New([]Option{
		WithHandleCrash(true),
	})

	assert.Equal(t, true, i.handleCrash)
}

func TestWithSession(t *testing.T) {
	name := "LinkAja"
	version := "0.0.1"
	port := 80

	i := New([]Option{
		WithSession(true, name, version, port),
	})

	assert.Equal(t, true, i.session)
	assert.Equal(t, name, i.name)
	assert.Equal(t, version, i.version)
	assert.Equal(t, port, i.port)
}

func TestWithInternalServerErrorMessage(t *testing.T) {
	internalServerErrorMessage := "Server Gangguan"

	i := New([]Option{
		WithInternalServerErrorMessage(internalServerErrorMessage),
	})

	assert.Equal(t, internalServerErrorMessage, i.internalServerErrorMessage)
}

func TestWithSkip(t *testing.T) {
	rpcs := []string{"/admin"}

	i := New([]Option{
		WithSkip(rpcs),
	})

	assert.Equal(t, rpcs[0], i.skipRPCs[0])
}
