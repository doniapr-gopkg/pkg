package interceptor

import (
	"context"
	"github.com/stretchr/testify/assert"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"google.golang.org/grpc/metadata"
	"testing"
)

type serverStream struct {
	ctx context.Context
}

func (s serverStream) SetHeader(metadata.MD) error {
	return nil
}

func (s serverStream) SendHeader(metadata.MD) error {
	return nil
}

func (s serverStream) SetTrailer(metadata.MD) {}

func (s serverStream) Context() context.Context {
	return s.ctx
}

func (s serverStream) SendMsg(m interface{}) error {
	return nil
}

func (s serverStream) RecvMsg(m interface{}) error {
	return nil
}

func newServerStream(ctx context.Context) *serverStream {
	return &serverStream{ctx: ctx}
}

func TestWrapServerStream(t *testing.T) {
	ctx := context.Background()
	wss := WrapServerStream(newServerStream(ctx))

	assert.NotNil(t, wss)
}

func TestWrappedServerStream_Context(t *testing.T) {
	ctx := context.Background()
	wss := WrapServerStream(newServerStream(ctx))

	assert.Equal(t, ctx, wss.Context())
}

func TestInterceptor_SetLogger(t *testing.T) {
	i := New([]Option{})

	logger := Logger.New(Logger.Options{
		FileLocation:    "",
		FileTdrLocation: "",
		FileMaxAge:      0,
		Stdout:          false,
	})

	i.SetLogger(logger)

	assert.Equal(t, logger, i.logger)
}

func TestInterceptor_SetDebug(t *testing.T) {
	i := New([]Option{})

	i.SetDebug(true)

	assert.Equal(t, true, i.debug)
}

func TestInterceptor_SetPort(t *testing.T) {
	i := New([]Option{})

	port := 80

	i.SetPort(port)

	assert.Equal(t, port, i.port)
}

func TestInterceptor_Stream(t *testing.T) {
	i := New([]Option{})

	assert.NotNil(t, i.Stream())
}

func TestInterceptor_Unary(t *testing.T) {
	i := New([]Option{})

	assert.NotNil(t, i.Unary())
}
