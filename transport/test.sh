#!/usr/bin/env bash

set -e

echo 'mode: count' >profile.cov

go test -v . -bench=.

for dir in $(find . -maxdepth 10 \
  -not -path './.git*' \
  -not -path '*/_*' \
  -not -path '.' \
  -not -path './vendor*' \
  -not -path './vendor*' \
  -type d); do
  if ls $dir/*.go &>/dev/null; then
    go test -v $dir
  fi
done
