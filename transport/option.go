package transport

import (
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"gitlab.com/doniapr-gopkg/pkg/transport/custom"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc"
	"gitlab.com/doniapr-gopkg/pkg/transport/http"
)

type Option func(*Transport)

func WithInherit(inherit bool) Option {
	return func(t *Transport) {
		t.inherit = inherit
	}
}

func WithLogger(logger Logger.Logger) Option {
	return func(t *Transport) {
		t.logger = logger
	}
}

func WithDebug(enabled bool) Option {
	return func(t *Transport) {
		t.debug = enabled
	}
}

func WithHTTPServer(httpServer *http.Server) Option {
	return func(t *Transport) {
		t.httpServer = httpServer
	}
}

func WithGRPCServer(grpcServer *grpc.Server) Option {
	return func(t *Transport) {
		t.grpcServer = grpcServer
	}
}

func WithCustom(service *custom.Holder) Option {
	return func(t *Transport) {
		t.services = append(t.services, service)
	}
}

func WithTracing(tracingName, jaegerHost string, jaegerPort uint16, probability float64) Option {
	return func(t *Transport) {
		t.tracing = true
		t.jaegerHost = jaegerHost
		t.jaegerPort = jaegerPort
		t.tracingName = tracingName
		t.tracingProbability = probability
	}
}
