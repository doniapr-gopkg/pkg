package transport

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/doniapr-gopkg/pkg/transport/grpc"
	"gitlab.com/doniapr-gopkg/pkg/transport/http"
	"testing"
)

func TestTransport_SetHTTP(t *testing.T) {
	h := http.New([]http.Option{})

	transport := New([]Option{})
	transport.SetHTTP(h)

	assert.Equal(t, h, transport.httpServer)
}

func TestTransport_SetGRPC(t *testing.T) {
	g := grpc.New([]grpc.Option{})

	transport := New([]Option{})
	transport.SetGRPC(g)

	assert.Equal(t, g, transport.grpcServer)
}

func TestTransport_SetupHTTP(t *testing.T) {
	transport := New([]Option{})
	transport.SetupHTTP([]http.Option{})

	assert.NotNil(t, transport.httpServer)
}

func TestTransport_SetupGRPC(t *testing.T) {
	transport := New([]Option{})
	transport.SetupGRPC([]grpc.Option{})

	assert.NotNil(t, transport.grpcServer)
}

func TestTransport_Run(t *testing.T) {
	transport := New([]Option{})
	err := transport.Run()

	assert.Nil(t, err)
}
