package custom

import (
	"context"
	Logger "gitlab.com/doniapr-gopkg/pkg/logger"
	"sync"
)

type Service interface {
	SetDebug(enabled bool)
	SetLogger(logger Logger.Logger)
	Start(ctx context.Context, wg *sync.WaitGroup) func() error
}
