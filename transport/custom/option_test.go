package custom

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/doniapr-gopkg/pkg/transport/example/kafka/service"
	"testing"
	"time"
)

func TestWithService(t *testing.T) {
	custom := service.New()

	h := New([]Option{
		WithService(custom),
	})

	assert.Equal(t, custom, h.service)
}

func TestWithHold(t *testing.T) {
	hold := 5 * time.Second

	h := New([]Option{
		WithHold(hold),
	})

	assert.Equal(t, hold, h.hold)
}
