package custom

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/doniapr-gopkg/pkg/transport/example/kafka/service"
	"math"
	"testing"
	"time"
)

func TestHolder_Service(t *testing.T) {
	custom := service.New()

	h := New([]Option{
		WithService(custom),
	})

	assert.Equal(t, custom, h.Service())
}

func TestHolder_Hold(t *testing.T) {
	hold := 1 * time.Second

	h := New([]Option{
		WithHold(hold),
	})
	now := time.Now()

	h.Hold()

	sub := time.Now().Sub(now)

	assert.Equal(t, math.Floor(sub.Seconds()), math.Floor(h.hold.Seconds()))
}
